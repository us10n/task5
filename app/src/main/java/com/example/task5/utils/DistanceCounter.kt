package com.example.task5.utils

import com.google.android.gms.maps.model.LatLng
import kotlin.math.pow
import kotlin.math.sqrt

object DistanceCounter {
    fun distanceBetween(pointA: LatLng, pointB: LatLng): Double {
        val x1 = pointA.latitude
        val x2 = pointB.latitude
        val y1 = pointA.longitude
        val y2 = pointB.longitude
        return sqrt((x1 - x2).pow(2) + (y1 - y2).pow(2))
    }
}