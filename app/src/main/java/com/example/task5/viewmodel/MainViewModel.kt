package com.example.task5.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.task5.adapter.AtmMachineAdapter
import com.example.task5.adapter.BankFilialAdapter
import com.example.task5.adapter.InfoboxAdapter
import com.example.task5.api.entity.AtmMachine
import com.example.task5.api.entity.BankFilial
import com.example.task5.api.entity.DistanceComparator
import com.example.task5.api.entity.Infobox
import com.example.task5.repository.Repository
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    private val atmMachineCoordsLiveData = MutableLiveData<List<LatLng>>()
    val atmMachineCoords: LiveData<List<LatLng>>
        get() = atmMachineCoordsLiveData

    private val errorHandlerLiveData = MutableLiveData(false)
    val errorHandler: LiveData<Boolean>
        get() = errorHandlerLiveData

    private val bankRelatedPlaces = mutableListOf<LatLng>()

    fun findAllAtmCoordinates(cityName: String) {
        Single.concat(
            atmMachinesDataSource(cityName),
            bankFilialsDataSource(cityName),
            infoboxesDataSource(cityName)
        )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnComplete {
                getNearestPlaces(bankRelatedPlaces)
            }
            .subscribe({ it ->
                it.forEach {
                    val point = when (it.javaClass) {
                        BankFilial::class.java -> BankFilialAdapter.instance.bankFilialToLatLng(it as BankFilial)
                        AtmMachine::class.java -> AtmMachineAdapter.instance.atmMachineToLatLng(it as AtmMachine)
                        Infobox::class.java -> InfoboxAdapter.instance.infoboxToLatLng(it as Infobox)
                        else -> return@forEach
                    }
                    bankRelatedPlaces.add(point)
                }
            }, {
                it.printStackTrace()
            })
    }

    private fun getNearestPlaces(
        places: List<LatLng>, amount: Int = 10
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val distanceComparator = DistanceComparator()
            val nearestPlaces = places.sortedWith(distanceComparator).distinct().take(amount)
            atmMachineCoordsLiveData.postValue(nearestPlaces)
        }
    }

    private fun atmMachinesDataSource(cityName: String): Single<List<AtmMachine>> =
        repository.getAllAtms(cityName)

    private fun bankFilialsDataSource(cityName: String): Single<List<BankFilial>> =
        repository.getAllBankFilials(cityName)

    private fun infoboxesDataSource(cityName: String): Single<List<Infobox>> =
        repository.getAllInfoboxes(cityName)
}