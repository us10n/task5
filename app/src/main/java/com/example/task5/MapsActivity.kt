package com.example.task5

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.task5.databinding.ActivityMapsBinding
import com.example.task5.viewmodel.MainViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    private val binding by lazy { ActivityMapsBinding.inflate(layoutInflater) }
    private val mainViewModel: MainViewModel by viewModels()

    companion object {
        private const val CITY_NAME = "Гомель"
        private const val ATM_TITLE = "Беларусбанк"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        mainViewModel.atmMachineCoords.observe(this) {
            showAtmMachines(it)
        }
        mainViewModel.errorHandler.observe(this) {
            if (it)
                Toast.makeText(this, "Data load error", Toast.LENGTH_LONG).show()
        }
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val gomel = LatLng(52.425163, 31.015039)
        val zoomLevel = 12f
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, zoomLevel))

        mainViewModel.findAllAtmCoordinates(CITY_NAME)
    }

    private fun showAtmMachines(atmCoordinates: List<LatLng>) {
        atmCoordinates.forEach {
            mMap?.addMarker(MarkerOptions().position(it).title(ATM_TITLE))
        }
    }

    override fun onStop() {
        mainViewModel.atmMachineCoords.removeObservers(this)
        super.onStop()
    }
}