package com.example.task5.repository

import com.example.task5.api.BankApi
import com.example.task5.api.entity.AtmMachine
import com.example.task5.api.entity.BankFilial
import com.example.task5.api.entity.Infobox
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class Repository @Inject constructor(
    private val bankApi: BankApi
) {
    fun getAllAtms(cityName: String): Single<List<AtmMachine>> = bankApi.getAllATMMachines(cityName)
    fun getAllInfoboxes(cityName: String): Single<List<Infobox>> = bankApi.getAllInfoboxes(cityName)
    fun getAllBankFilials(cityName: String): Single<List<BankFilial>> = bankApi.getAllBankFilials(cityName)
}