package com.example.task5.api

import com.example.task5.api.entity.AtmMachine
import com.example.task5.api.entity.BankFilial
import com.example.task5.api.entity.Infobox
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BankApi {

    private companion object {
        const val atmUrl = "./atm"
        const val infoboxUrl = "./infobox"
        const val filialUrl = "./filials_info"
    }

    @GET(atmUrl)
    fun getAllATMMachines(@Query("city") cityName: String): Single<List<AtmMachine>>

    @GET(infoboxUrl)
    fun getAllInfoboxes(@Query("city") cityName: String): Single<List<Infobox>>

    @GET(filialUrl)
    fun getAllBankFilials(@Query("city") cityName: String): Single<List<BankFilial>>

}