package com.example.task5.api.entity

data class Infobox(
    val address: String,
    val address_type: String,
    val area: String,
    val cash_in: String,
    val cash_in_exist: String,
    val city: String,
    val city_type: String,
    val currency: String,
    val gps_x: String,
    val gps_y: String,
    val house: String,
    val inf_printer: String,
    val inf_status: String,
    val inf_type: String,
    val info_id: Int,
    val install_place: String,
    val location_name_desc: String,
    val popolnenie_platej: String,
    val region_platej: String,
    val time_long: String,
    val type_cash_in: String,
    val work_time: String
)