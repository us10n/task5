package com.example.task5.api.entity

data class GeographicalPoint(
    var x: Double,
    var y: Double
){

}