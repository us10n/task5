package com.example.task5.adapter

import com.example.task5.api.entity.AtmMachine
import com.google.android.gms.maps.model.LatLng

class AtmMachineAdapter {
    companion object {
        val instance = AtmMachineAdapter()
    }

    fun atmMachineToLatLng(atmMachine: AtmMachine): LatLng =
        LatLng(atmMachine.gps_x.toDouble(), atmMachine.gps_y.toDouble())
}